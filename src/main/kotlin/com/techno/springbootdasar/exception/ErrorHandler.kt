package com.techno.springbootdasar.exception

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResFormError
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.lang.RuntimeException

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ErrorHandler {
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleArgumentValidException(exception: MethodArgumentNotValidException): ResponseEntity<ResBaseDto<ResFormError>> {
//        val errors = mutableListOf<String>()
//        exception.bindingResult.fieldErrors.forEach{
//            errors.add(it.defaultMessage!!)
//        }

        val fields = exception.fieldErrors.associate { it.field to it.defaultMessage }
        val global = exception.globalErrors.map { it.defaultMessage }
        val errors = ResFormError(
            fieldError = fields,
            globalError = global
        )

        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = errors
        )

        return ResponseEntity.badRequest().body(result)
    }

    @ExceptionHandler(RuntimeException::class)
    fun handleCustomException(exception: RuntimeException): ResponseEntity<Any>{
        exception.printStackTrace()

        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = exception.message
        )

        return ResponseEntity.badRequest().body(result)
    }
}