package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqSignInDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.service.AuthenticationService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/v1/api/account/login")
class AuthenticationController(
    private val authenticationService: AuthenticationService
) {

    @PostMapping
    fun signIn(@RequestBody reqSignInDto: ReqSignInDto): ResponseEntity<ResBaseDto<Any>> {
        val response = authenticationService.signIn(reqSignInDto)

        return if (response.outStat)
            ResponseEntity.ok().body(response)
        else
            ResponseEntity.badRequest().body(response)
    }
}