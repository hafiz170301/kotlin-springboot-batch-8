package com.techno.springbootdasar.domain.dto.request

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ReqSignInDto (
    @field:NotNull(message = "Email cannot be null")
    @field:NotEmpty(message = "Email cannot be blank")
    @field:JsonProperty(value = "email")
    val email: String = "",

    @field:NotNull(message = "password cannot be null")
    @field:NotEmpty(message = "password cannot be blank")
    @field:JsonProperty(value = "password")
    val password: String = ""
)