package com.techno.springbootdasar.domain.dto.request

import javax.validation.constraints.Negative
import javax.validation.constraints.NegativeOrZero
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Positive
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

data class ReqDataDto (
    @field:NotBlank(message = "notNullRequest is Not Blank")
    val notNullRequest: String?,

    @field:NotEmpty(message = "notEmptyRequest is Not Empty")
    val notEmptyRequest: String?,

    @field:NotNull(message = "minMaxRequest is Not Null")
    @field:NotEmpty(message = "minMaxRequest is Not Empty")
    @field:Size(min = 5, max = 10, message = "minMaxRequest betweeen 5 to 10")
    val minMaxRequest: String?,

    @field:Positive(message = "positiveRequest Only Positive")
    val positiveRequest: Int?,

    @field:PositiveOrZero(message = "positiveOrZeroRequest")
    val positiveOrZeroRequest: Int?,

    @field:Negative(message = "negativeRequest")
    val negativeRequest: Int?,

    @field:NegativeOrZero(message = "negativeOrZero")
    val negativeOrZeroRequest: Int?,

    @field:NotNull(message = "patternRequest is Not Null")
    @field:NotEmpty(message = "patternRequest is Not Empty")
    @field:Pattern(regexp = "^[a-zA-z]*$", message = "patterRequest is Invalid Pattern")
    val patternRequest: String?,
)