package com.techno.springbootdasar.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty

data class ResSigninDto (

    @field:JsonProperty(value = "id_user")
    val idUser: String? = "",

    @field:JsonProperty(value = "email")
    val email: String? = "",

    @field:JsonProperty(value = "token")
    val token: String? = ""
)