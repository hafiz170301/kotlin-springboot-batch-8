package com.techno.springbootdasar.domain.dto.response

import com.fasterxml.jackson.annotation.JsonProperty

data class ResFormError (
    @field:JsonProperty(value = "field_errors")
    val fieldError: Any? = null,

    @field:JsonProperty(value = "global_errors")
    val globalError: Any? = null
)