package com.techno.springbootdasar.domain.entity

import java.io.Serializable
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "mst_role")
data class RoleEntity (

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @field:Column(name = "id_role", columnDefinition = "uuid")
    val idRole: UUID? = null,

    @field:Column(name = "name", columnDefinition = "varchar")
    val name: String? = null
): Serializable