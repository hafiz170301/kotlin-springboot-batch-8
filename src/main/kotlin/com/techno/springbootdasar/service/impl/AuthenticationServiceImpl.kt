package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.request.ReqSignInDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResSigninDto
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.service.AuthenticationService
import com.techno.springbootdasar.util.JwtGenerator
import org.springframework.stereotype.Service

@Service
class AuthenticationServiceImpl (
    private val userRepository: UserRepository
): AuthenticationService {

    override fun signIn(reqSignInDto: ReqSignInDto): ResBaseDto<Any> {
        val data = userRepository.findByEmailAndPassword(reqSignInDto.email, reqSignInDto.password) ?:
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = "Invalid Username or Password"
            )

        val token = JwtGenerator().createJWT(
            data.idUser.toString(),
            reqSignInDto.email
        )

        val response = ResSigninDto(
            idUser = data.idUser.toString(),
            email = data.email,
            token = token
        )

        return ResBaseDto(data = response)
    }

}