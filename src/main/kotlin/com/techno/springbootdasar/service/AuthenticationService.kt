package com.techno.springbootdasar.service

import com.techno.springbootdasar.domain.dto.request.ReqSignInDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto

interface AuthenticationService {
    fun signIn(reqSignInDto: ReqSignInDto): ResBaseDto<Any>
}