package com.techno.springbootdasar.repository

import com.techno.springbootdasar.domain.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

interface UserRepository: JpaRepository<UserEntity, String> {
    // native query
    fun findByIdUser(idUser: UUID?): UserEntity?
    fun findByEmailAndPassword(email: String?, password: String?): UserEntity?

    // raw query
    @Query("SELECT a FROM UserEntity a WHERE idUser = :idUser")
    fun getByIdUser(idUser: UUID): UserEntity?

    // native raw query
    @Query("SELECT * FROM mst_user mu WHERE mu.uuid = :idUser", nativeQuery = true)
    fun getByIdUserNative(idUser: UUID): List<String>

    @Modifying
    @Transactional
    fun deleteByIdUser(idUser: UUID): Int?
}